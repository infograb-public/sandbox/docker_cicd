# Use an official Ubuntu as a parent image
FROM ubuntu:20.04

# Install nginx
RUN apt-get update \
    && apt-get install -y nginx

# Remove default nginx page
RUN rm -rf /var/www/html/*

# Copy the application files to the container
COPY ./html /var/www/html

# Make port 80 available outside this container
EXPOSE 80

# Run Nginx when the container launches, in non-daemon mode
CMD ["nginx", "-g", "daemon off;"]
